package com.elastic.search.entities;

import lombok.Data;

/**
 * @author: huangdeyao
 * @create: 2018-08-21 18:51
 **/
@Data
public class Article {
    private String author;
    private String content;
    private String title;
}
