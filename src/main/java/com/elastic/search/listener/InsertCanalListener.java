package com.elastic.search.listener;

import com.alibaba.otter.canal.protocol.CanalEntry.RowData;
import com.elastic.search.event.InsertCanalEvent;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

/**
 * insert
 * @author huangdeyao
 */
@Component
public class InsertCanalListener extends AbstractCanalListener<InsertCanalEvent> {

    private static final Logger logger = LoggerFactory.getLogger(InsertCanalListener.class);

    @Autowired
    RestHighLevelClient restHighLevelClient;

    @Override
    protected void doSync(String database, String table, String index, RowData rowData) {
        logger.info("==================================Insert============================================");
        logger.info("database:" + database + ",table:" + table + ",index:" + index + ",rowData:" + rowData);

        IndexRequest indexRequest = new IndexRequest(index);
        Map<String, Object> jsonMap = parseColumnsToMap(rowData.getAfterColumnsList());
        if (jsonMap.get(ES_ID) != null) {
            indexRequest.id(jsonMap.get(ES_ID).toString());
            indexRequest.source(jsonMap);
            indexRequest.timeout("1s");
            try {
                IndexResponse response = restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
                logger.info("id==>" + jsonMap.get(ES_ID) + "======== 实时同步数据 insert 结果 ======>" + response.status());
                logger.info(response.toString());
            } catch (IOException e) {
                logger.info("================= 实时同步数据 insert 异常======");
                e.printStackTrace();
            }
        } else {
            logger.info("================= 实时同步数据 insert 没有表id字段======");
        }
    }
}
