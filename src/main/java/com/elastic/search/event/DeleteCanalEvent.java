package com.elastic.search.event;

import com.alibaba.otter.canal.protocol.CanalEntry.Entry;

/**
 * @author huangdeyao
 */
public class DeleteCanalEvent extends CanalEvent {
    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public DeleteCanalEvent(Entry source) {
        super(source);
    }
}
