package com.elastic.search.event;

import com.alibaba.otter.canal.protocol.CanalEntry.Entry;


/**
 * @author huangdeyao
 */
public class InsertCanalEvent extends CanalEvent {
    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public InsertCanalEvent(Entry source) {
        super(source);
    }
}
